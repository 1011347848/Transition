package com.weicong.transition;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.AutoTransition;
import android.transition.Scene;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {

    /**
     * scenes to transition
     */
    private Scene scene1, scene2;

    /**
     * transition to move between scenes
     */
    private Transition transition;

    /**
     * flag to swap between scenes
     */
    private boolean start;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get the layout ID
        RelativeLayout baseLayout = (RelativeLayout) findViewById(R.id.base);

        //first scene
        ViewGroup startView = (ViewGroup) getLayoutInflater()
                .inflate(R.layout.activity_main, baseLayout, false);

        //second scene
        ViewGroup endView = (ViewGroup) getLayoutInflater()
                .inflate(R.layout.activity_end, baseLayout, false);

        //create the two scenes
        scene1 = new Scene(baseLayout, startView);
        scene2 = new Scene(baseLayout, endView);

        //create transition, set properties
        transition = new AutoTransition();
        transition.setDuration(5000);
        transition.setInterpolator(new AccelerateDecelerateInterpolator());

        //initialize flag
        start = true;
    }

    public void changeScene(View v) {
        //check flag
        if (start) {
            TransitionManager.go(scene2, transition);
            start = false;
        } else {
            TransitionManager.go(scene1, transition);
            start = true;
        }
    }
}
